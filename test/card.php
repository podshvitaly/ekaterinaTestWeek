<div class="card">
    <div class="card-image">
        <img src="<?= $product["PROPERTIES"][ $data["PROPERTIES"]['MORE_PHOTO']['ID'] ][0] ?>" alt="image of product">
        <div class="card-image__id"><span>Код: <?= $product["XML_ID"] ?></span></div>
    </div>
    <div class="card__txt-wrapper">
        <h4 class="card__heading"><?= $product["NAME"] ?></h4>
        <?php foreach ($showProperties as $code) {
            $property = $data["PROPERTIES"][$code];
            ?> <p class="card__txt"> <?= $data["PROPERTIES"][$code]["NAME"] ?>
                : <?php
                if($property['MULTIPLE'] == 'Y'){
                    switch ($property['USER_TYPE']){
                        case 'directory':
                            $columName = 'UF_NAME';
                            break;
                        default:
                            $columName = 'NAME';
                    }
                    echo implode(', ', array_column($product["PROPERTIES"][$property['ID']], $columName));

                }else{
                    echo $product["PROPERTIES"][$property['ID']]["NAME"];
                }
                ?>
            </p> <?php
        }
        ?>
        <div class="card-btn">
            <a href="<?= $product["URL"] ?>" class="card__btn">Купить</a>
        </div>
    </div>
</div>












