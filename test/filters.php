<?php
$showFilters = [
    "COLOR_REF",
    "COUNTRY_REF",
    "MANUFACTURER",
    "FORMA",
    "SURFACE"
];

$filter = function ($arr) {
    $properties = [
        "colArr" => [], //"colArr" => [[id(int) => "коричневый"],
                        //            [id(int) => "бежевый"]]
        "countryArr" => [],
        "brandArr" => [],
        "formArr" => [],
        "textureArr" => []
    ];

    foreach ($arr as $productData) {

        foreach ( $productData["PROPERTIES"][42] as $item) {
            $properties["colArr"][ $item['ID']] = $item["UF_NAME"];
        }


        $properties["countryArr"][$productData["PROPERTIES"][60]["ID"]] =
            $productData["PROPERTIES"][60]["UF_NAME"];

        $properties["brandArr"][$productData["PROPERTIES"][64]["ID"] ] =
            $productData["PROPERTIES"][64]["NAME"];

        foreach ( $productData["PROPERTIES"][47] as $item) {
            $properties["formArr"][ $item['ID']] = $item["NAME"];
        }

        foreach ( $productData["PROPERTIES"][58] as $item) {
            $properties["textureArr"][ $item['ID']] = $item["NAME"];
        }
    }

    return $properties;
};

$properties = $filter($productsData);

$combinedFilters = array_combine($showFilters, $properties);

//$combinedFilters = [
//    "COLOR_REF" => [id(число) => "коричневый",
//                    id(число) => "бежевый"]
//    "COUNTRY_REF" => [id(число) => "бельгия",
//                      id(число) => "россия"]
//    "MANUFACTURER",
//    "FORMA",
//    "SURFACE"
//];





