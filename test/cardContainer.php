<section class="wrapper" xmlns="http://www.w3.org/1999/html">
    <form method="post" action="" class="filter-block">

        <?php    //manufacturer, color_ref...
            foreach( $showFilters as $filter) {
                //номер свойства в объекте продукта
                $filterCode = $data["PROPERTIES"][$filter]["ID"];
                //название свойства (коричневый, бельгия и тд)
                $filterName = $data["PROPERTIES"][$filter]["NAME"];
                //массив из кл-зн число-свойство
                //[id(число) => "коричневый",
                //id(число) => "бежевый"]
                $filterContent = $combinedFilters[$filter];


            ?>

                <div class="filter-prop">
                    <h3 class="filter__heading"><?= $filterName ?></h3>
                    <ul class="filter-list">
                        <?php

                        //массив  конкретного фильтра и разбиваем на ключ-знач
                        //берем один массив: id(число) => "коричневый"
                        foreach ( $filterContent as $id => $value) {
                            ?> <li class="filter-list__item"><label for="<?=$filter?>_<?=$id?>">
                                    <input type="checkbox" name="COLOR_REF[]" value="<?=$id?>"
                                   id="<?=$filter?>_<?=$id?>"> <?= $value ?></label></li>

                            <?php



                            //input: name="COLOR_REF[здесь что-то]"
                            //       value="число ID цвета (из продактса)"
                        }
                        ?>
                    </ul>
                </div>
            <?php
            }
        ?>

        <div class="filter__submit-block">
            <label for="filter__btn"><input id="filter__btn" type="submit" name="button" class="filter__btn">Применить настройки</label>
        </div>

    </form>

    <section class="container">
<?php






//$filteredData = [];
//$filteredData['COLOR_REF'][] = 53;
//$filteredData['COLOR_REF'][] = 64;






//if(isset($_POST["COLOR_REF[]"])) {
    //берем объект каждого продукта
    foreach ($productsData as $product) {
        $found = [];


        //
        if(!empty($_POST["COLOR_REF[]"])) {

            //для каждой даты "color_ref"($prortiCOde) => [53, 64]($values)
            foreach ($_POST["COLOR_REF[]"] as $propertyCode => $values) {



                //массив со свойствами из пропертис
                $property = $data["PROPERTIES"][$propertyCode];

                //find = ["COLOR_REF" => false,]
                $found[$propertyCode] = false;

                //для каждого из [53, 64] из фильтердата
                foreach ($values as $value) {

                    //проверка на множественное значение
                    if ($property['MULTIPLE'] == 'Y') {

                        //
                        //        массив из продуктов с конкрет айди [0]     сам массив
                        foreach ($product['PROPERTIES'][$property['ID']] as $__propertyValue) {
                            //№id из продакта,напр цвета = из ПОСТА дата => find = ["COLOR_REF" => true,]
                            if ($__propertyValue['ID'] == $value) $found[$propertyCode] = true;
                            break;
                        }
                        if ($found[$propertyCode]) break;
                    }
                    //если айди в продактс == значению из ПОСТ
                    if ($product['PROPERTIES'][$property['ID']]['ID'] == $value) {
                        $found[$propertyCode] = true;
                        break;
                    }

                }
                //include("card.php");

            }

            $isNotFilter = false;
            //find = ["COLOR_REF" => true,...]
            foreach ($found as $value) if (!$value) {
                $isNotFilter = true;
                break;
            }
        }

        if ($isNotFilter) continue;
        include("card.php");

    }
//}






?>
    </section>
</section>


<!---->
//<?php
//
//foreach($productsData as $product) {
//    if( $product["ACTIVE"] === "Y") {
//        include("card.php");
//    }
//}
//?>

